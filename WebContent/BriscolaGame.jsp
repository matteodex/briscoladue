<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page session="true" import="java.util.*" %>

<% String player = (String) session.getValue("playerName"); %>
<% String ip = (String) session.getValue("ipAddr"); %>
<% String port = (String) session.getValue("portNum"); %>
<!DOCTYPE html>
<html ng-app="myApp">
	<head>
	<meta charset="ISO-8859-1">
	<title>Briscola</title>
	
	<style>
		.enemy_hand, .board, .hand {
			height: 180px;
		}
		
		li{
			display: inline;
			margin-right: 50px;
		}
		li img{
			height: 180px;
			width: auto;
			border: 1px solid black;
			border-radius: 10px;
		}
		.hand li img:hover {
			cursor: pointer;
			opacity: 0.5;
		}
		.brisc {
			position: absolute;
			top: 300px;
			left: -5px;
			border: 1px solid black;
			border-radius: 10px;
		}
	</style>
	
	</head>

<body ng-controller="gametCtrl as ctrl" bgcolor=#006600>
	
	Hello <% out.println(player); %>
	
		Turn:<span ng-bind="ctrl.turn"></span>
		<img class="brisc" ng-src="{{ctrl.briscola.url_image}}" width=50px height="auto"></span>
		Points:<span ng-bind="ctrl.points"></span>
		<!-- PointsAI:<span ng-bind="ctrl.pointsAI"></span> -->
		Remaining Cards in Deck:<span ng-bind="ctrl.leftCards"></span>

		
		<div  class="enemy_hand">
		<ul>
			
			  <li ng-repeat="item in ctrl.opponentHand">
			   <img ng-src="{{item.back}}"/>  

			  </li>
		</ul>
		</div>
		
		<div class="board" >
			<ul>
				  <li ng-repeat="x in ctrl.board">
					<img ng-src="{{x.url_image}}"/>
				  </li>
			</ul>
		</div>
		<div class="hand">
			<ul>
				  <li ng-repeat="x in ctrl.hand" ng-click="ctrl.play_card($index)">
					<img ng-src="{{x.url_image}}" width=50px height="auto" />
				  </li>
			</ul>
		</div>
		
		<button ng-click="ctrl.update_status()">Update</button>
		
		
	
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.19/angular.js"></script>
	<script>
		var app = angular.module('myApp', [])
			.controller('gametCtrl', [ '$http', function($http) {
				
				/*
				var self = this;
				
				
				self.cards = [];
				self.turn = 0;
				// get complete items list
				$http.get("http://<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=gethand&player=<% out.print(player);%>")
					.success(function(data) {
						self.cards = data;
						//alert(self.items);
					});
				$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getturn&player=<% out.print(player);%>")
				.success(function(data) {
					self.turn = data;
					//alert(self.items);
				});
				
				self.refresh_turn = function(){
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getturn&player=<% out.print(player);%>")
					.success(function(data) {
						self.turn = data;
						
					});
				}
				
				// Run function every second
				//setInterval(self.refresh_turn, 1000);
				
				*/
				
			
				var self = this;
				self.hand = [];
				self.board = [];
				self.briscola = [];
				self.turn = 0;
				self.points = 0;
				self.pointsAI = 0;
				self.opponentHand =[];
				self.leftCards=40;
						
				self.update_status = function(){
					// action = getTurn
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getTurn&player=<% out.print(player);%>")
					.success(function(data) {
						self.turn = data;;
					});
					
					// action = getHand
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getHand&player=<% out.print(player);%>")
					.success(function(data) {
						self.hand = data;
					});
					
					// action = getOpponentHand
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getOpponentHand&player=<% out.print(player);%>")
					.success(function(data) {
						self.opponentHand = data;
					});
					
					// action = getLeftCards
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getLeftCards&player=<% out.print(player);%>")
					.success(function(data) {
						self.leftCards = data;
					});
					// action = getBoard
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getBoard&player=<% out.print(player);%>")
					.success(function(data) {
						self.board = data;
					});
					
					// action = getPoints
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getPoints&player=<% out.print(player);%>")
					.success(function(data) {
						self.points = data;
					});
					
					// action = getPointsAI
					$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getPointsAI&player=<% out.print(player);%>")
					.success(function(data) {
						self.pointsAI = data;
					});
				}
				
				// action getBriscola
				$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=getBriscola&player=<% out.print(player);%>")
				.success(function(data) {
					self.briscola = data;
				});
				
				self.update_status();
				
				self.play_card = function(index){
					//alert(index);
					if(self.turn == 1)
						$http.get("http:///<% out.print(ip);%>:<% out.print(port);%>/Briscola_Due_Servlet/BriscolaDueServlet?game=1&action=playcard&card=" + index + "&player=<% out.print(player);%>");
				}
				
				
				// Run function every  2 second
				setInterval(self.update_status, 3000);
				
			}]);
	</script>
</body>
</html>