package briscola;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.Gson;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;


@WebServlet("/BriscolaDueServlet")
public class BriscolaDueServlet extends HttpServlet {
	
	public static final String ip = "25.64.4.107";
	public static final String port = "8080";
	//ciaociaociao
	private static final long serialVersionUID = 1L;
       
	private Player player1,player2;
	private Game g = null;
	private Player current,winner;
	private boolean play_with_AI=false;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BriscolaDueServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(request.getParameterMap().containsKey("player") && !request.getParameterMap().containsKey("game")){
			
			// just one player, so force player1 to refresh
			if(player2 == null){
				response.getWriter().println("Waiting for another player");
				response.setHeader("Refresh", "2; URL=http://"+ip+":"+port+"/Briscola_Due_Servlet/BriscolaDueServlet?player=" + player1.getName());
			}
			
			// two players: start the game
			
			else {
					String name = "unknown";
					if(request.getParameterMap().containsKey("player"))
						name = request.getParameter("player");
					//g = new Game(player1,player2);
					//System.out.println(g.getDeck().printDeck());
					//response.sendRedirect("BriscolaGame.html?player="+request.getParameter("player"));
					HttpSession session = request.getSession(true);
					if(name != null && session != null){
						session.setAttribute("playerName", name);
						session.setAttribute("ipAddr", ip);
						session.setAttribute("portNum", port);
					}
					String url="/BriscolaGame.jsp?player="+name;
					ServletContext sc = getServletContext();
					RequestDispatcher rd = sc.getRequestDispatcher(url);
	
				    rd.forward(request, response);
		
				
			}
		} else {
			//System.out.print("Received a req");
			String name,action;
			Player p = player1;
			
			if(( name = request.getParameter("player")) != null){
				if(player1.getName().equals(name)){
					//system.out.println(" form player1: " + name);
					p = player1;
				}
				if(player2.getName().equals(name)){
					//system.out.print(" from player2: " + name);
					p = player2;
				}
				
				
				if((action = request.getParameter("action"))!=null){
					Gson gson = new Gson();
					String json = null;
				
					//system.out.print(" requested action " + action );
					
					if(action.equals("getTurn")){
						if(current == p)
							json = gson.toJson(1);
						else json = gson.toJson(0);
						//system.out.println(current.getName() + " " +p.getName() + " " + json);
					}
					
					if(action.equals("getPoints")){
						json = gson.toJson(p.getPoints());
					}
					if(action.equals("getPointsAI")){
						json = gson.toJson(player2.getPoints());
					}
					if(action.equals("getOpponentHand")){
						if(p == player1){
							json = gson.toJson(player2.sizeHand());
						}	
						else{
							json = gson.toJson(player1.sizeHand());
						}
					}
					if(action.equals("getHand")){
						json = gson.toJson(p.getHand());
					}
					if(action.equals("getLeftCards")){
						json = gson.toJson(g.getDeck().getCardsLeft());
					}
					if(action.equals("getBoard")){
						json = gson.toJson(g.getBoard());
					}
					if(action.equals("getBriscola")){
						//json = gson.toJson(g.getBriscola());
						json = gson.toJson(g.getBriscola_card());
					}
					if(action.equals("playcard")){
						//system.out.println( name + "played the card " + request.getParameter("card"));
						g.playTurn(Integer.parseInt(request.getParameter("card")));
						
						// AI is second to play
						if(play_with_AI == true && g.getTurnCount() == 1){
							g.playTurn(AIchooseCardForSecond(g.getBoard().get(0),player2.getHand()));
							//g.playTurn(new Random().nextInt(player2.getHand().size()));	
						}
						
						// wait 3 seconds before empty the board
						try {
							TimeUnit.SECONDS.sleep(3);
						} catch (InterruptedException e) {e.printStackTrace();}
						
						winner = g.endTurn();
						current = g.getTurn();
						
						// AI is first to play because he won the last turn
						if(play_with_AI == true && current == player2){
							//g.playTurn(new Random().nextInt(player2.getHand().size()));	
							g.playTurn(AIchooseCardForFirst(player2.getHand()));
							current = player1;
						}
						
					}
					
					if(json != null){
						response.setContentType("text/json");
					    response.setCharacterEncoding("UTF-8");
					    response.getWriter().write(json); 
					}
				}
			}
			
			
		}
	}

	private int AIchooseCardForFirst(List<Card> hand) {
		int cardToPlay=playLiscio(hand,g.getBriscola_card().getSuit()); //se possibile gioca liscio
		
		if(g.getDeck().getCardsLeft()==2 && g.getPlayerTwo().getPoints()>45 && g.getPlayerOne().getPoints() < 49){
			cardToPlay=playCarico(hand,g.getBriscola_card().getSuit()); //se possibile gioca liscio
			return cardToPlay;
		}
		
		if((g.numBriscolePlayed() >= 5 && g.getDeck().getCardsLeft()>20) ||
		   (g.numBriscolePlayed() >= 7 && g.getDeck().getCardsLeft()<15) ||
		   (g.numBriscolePlayed() >= 8 && g.getDeck().getCardsLeft() < 4)  )
			{
			
			boolean probability=new Random().nextBoolean();
			if(probability){
				cardToPlay=playCarico(hand,g.getBriscola_card().getSuit());  //se possibile gioca Carico
				return cardToPlay;
			}
		}
		int aleo=new Random().nextInt(10);
		{
			if(aleo<=1){
				playCarico(hand,g.getBriscola_card().getSuit());
			}
		}
		
		return cardToPlay;
	}

	private int playCarico(List<Card> hand, String suit) {
		int cardToPlay=findMinimumCard(hand, suit);
		int counter=0;
		for(Card c:hand){
			if(!c.getSuit().equals(suit) && Integer.parseInt(c.getNumber())>=8){
				cardToPlay=counter;
			}
			counter++;
		}
		
		return cardToPlay;
		
	}

	private int playLiscio(List<Card> hand,String suit) {
		Card min = new Card("NoSuit","10"," ");
		Card minNotBriscola = new Card("NoSuit","10"," ");
		int counter=0;
		int cardToPlay=new Random().nextInt(hand.size());
		int cardToPlayNotBriscola=-1;
		for(Card c:hand){
			if((Integer.parseInt(c.getNumber())<Integer.parseInt(min.getNumber()))){
				min=c;
				cardToPlay=counter;
				if(!c.getSuit().equals(suit) && Integer.parseInt(c.getNumber()) <=7 ){ // la minore,meglio se non briscola
					minNotBriscola=c;
					cardToPlayNotBriscola=counter;
				}
				
			}
			counter++;
		}
		if(cardToPlayNotBriscola > -1)
		{
			if(Integer.parseInt(hand.get(cardToPlayNotBriscola).getNumber())>=6 && Integer.parseInt(hand.get(cardToPlay).getNumber())<=3) //se la carta non briscola � punti e la briscola � piccina gioca la piccina
			{
				return cardToPlay;
			}else
				return cardToPlayNotBriscola;
		}	
		else{
			return cardToPlay;
		}
			
		
	}

	private int AIchooseCardForSecond(Card player1Card, List<Card> hand) {
		Card max=new Card("NoSuit","-1","");
		int counter=0;
		int cardToPlay=findMinimumCard(hand,g.getBriscola_card().getSuit());;
		/*Se la carta dell'avversario � di briscola => carta minima
		 * altrimenti gioca la carta pi� alta dello stesse seme che ha nella mano,
		 * se non c'� gioca carta minima della mano
		 */
		int indexTake=isTakeable(hand,player1Card);
		int indexLose=isLosable(hand,player1Card);
		if(indexTake!=-1){//se riesce a fare 61 prendendo --> vince
			if((hand.get(indexTake).getScore()+player2.getPoints()+player1Card.getScore())>60){ 
				cardToPlay=indexTake;
				return cardToPlay;
			}
		}
		if((g.getDeck().getCardsLeft()==2 && indexLose!=-1) && //se ultima mano ed � perdibile e..
			((Integer.parseInt(g.getBriscola_card().getNumber())>=7) || //se briscola >= re o...
			(g.numBriscolePlayed()>=6 && g.getPlayerTwo().getPoints()<40))){
				cardToPlay=indexLose;
			}
		//TODO is prendibile
		
		if(!player1Card.getSuit().equals(g.getBriscola_card().getSuit())){ //se player1 non gioca briscola
			if(Integer.parseInt(player1Card.getNumber())>=8){ //se player1 gioca un carico (value 8-9)
				cardToPlay=tryTakeCarico(hand,player1Card); //prende carico se pu� anche usando briscola, altrimenti minima
				return cardToPlay;
			}
			else { //se player1 non gioca un carico 
				for(Card c : hand){
					if(c.getSuit().equals(player1Card.getSuit())){ //se carta in mano � stesso seme della carta dell'avversario
						if(Integer.parseInt(c.getNumber())>=5 && (Integer.parseInt(c.getNumber())>Integer.parseInt(player1Card.getNumber()))){ //se AI riesce a prendere con punti altrimenti lascia con minimum card
							if(Integer.parseInt(max.getNumber()) < Integer.parseInt(c.getNumber())){
								max=c;
								cardToPlay=counter;
							}
						}
					}
					counter++;
				}
				if(max.equals(new Card("NoSuit","-1",""))){//se non � stata trovata carta che superava 
					cardToPlay=findMinimumCard(hand,g.getBriscola_card().getSuit());	
					//ancora da fare case: un carico in mano e due briscola, per ora sceglie carico
				}
				return cardToPlay;
			}
			
		}
		else if(Integer.parseInt(player1Card.getNumber())==8){ //caso specifico che player1 giochi 3 briscola
			cardToPlay=isAceBriskInHand(hand,g.getBriscola_card().getSuit());
		}
		else{
			cardToPlay=playLiscio(hand, g.getBriscola_card().getSuit());
		}
		
		return cardToPlay;
	}

	private int isLosable(List<Card> hand, Card player1Card) {
		Card min = new Card("NoSuit","10"," ");
		int counter=0;
		int cardToPlay=-1;
		for(Card c : hand){
			if(player1Card.compareTo(c) > 0){ //se carta in mano � stesso seme della carta dell'avversario
				if(Integer.parseInt(c.getNumber())<Integer.parseInt(min.getNumber())){
					min=c;
					cardToPlay=counter;
				}
			}
			counter++;
		}
		
		return cardToPlay;

	}

	private int isTakeable(List<Card> hand, Card player1Card) {
		Card max = new Card("NoSuit","-1"," ");
		int counter=0;
		int cardToPlay=-1;
		for(Card c : hand){
			if(player1Card.compareTo(c) < 0){ //se carta in mano � stesso seme della carta dell'avversario
				if(Integer.parseInt(c.getNumber())>Integer.parseInt(max.getNumber())){
					max=c;
					cardToPlay=counter;
				}
			}
			counter++;
		}
		
		return cardToPlay;
	}

	private int isAceBriskInHand(List<Card> hand, String suit) {
		int counter=0;
		if(isAnyBriskInHand(hand, suit)){
			for(Card c : hand){
				if(c.getSuit().equals(suit) && Integer.parseInt(c.getNumber())==9){
					return counter;
				}
				counter++;
			}
		}
		return playLiscio(hand, g.getBriscola_card().getSuit());
	}

	private int tryTakeCarico(List<Card> hand,Card player1Card) {
		Card max = new Card("NoSuit","-1"," ");
		int counter=0;
		int cardToPlay=findMinimumCard(hand,g.getBriscola_card().getSuit());
		if(isAnyBriskInHand(hand,g.getBriscola_card().getSuit())){
			cardToPlay=findMinimumBriscola(hand,g.getBriscola_card().getSuit());
		}
		for(Card c : hand){
			if(c.getSuit().equals(player1Card.getSuit())){ //se carta in mano � stesso seme della carta dell'avversario
				if(Integer.parseInt(c.getNumber())>Integer.parseInt(player1Card.getNumber())){
					if(Integer.parseInt(max.getNumber()) < Integer.parseInt(c.getNumber())){
						max=c;
						cardToPlay=counter;
						}
				}
			}
			counter++;
		}
			
			
		
		return cardToPlay;
		
	}

	private int findMinimumBriscola(List<Card> hand, String suit) {
		Card min = new Card("NoSuit","10"," ");
		int counter=0;
		int cardToPlay=new Random().nextInt(hand.size());
		for(Card c : hand){
			if(c.getSuit().equals(suit)){ //se carta in mano � di briscol
				if(Integer.parseInt(c.getNumber())<Integer.parseInt(min.getNumber())){
					min=c;
					cardToPlay=counter;
				}
			}
			counter++;
		}
		return cardToPlay;
	}
	private int findMaximumBriscola(List<Card> hand, String suit) {
		Card max = new Card("NoSuit","-1"," ");
		int counter=0;
		int cardToPlay=-1;
		for(Card c : hand){
			if(c.getSuit().equals(suit)){ //se carta in mano � di briscol
				if(Integer.parseInt(c.getNumber())>Integer.parseInt(max.getNumber())){
					max=c;
					cardToPlay=counter;
				}
			}
			counter++;
		}
		return cardToPlay;
	}

	private boolean isAnyBriskInHand(List<Card> hand, String suit) {
		
		for(Card c : hand){
			if(c.getSuit().equals(suit))
				return true;
		}
		return false;
	}
	private List<Card> findBriscoleInHand(List<Card> hand, String suit){
		List<Card> listBriscole = null;
		for(Card c : hand){
			if(!c.getSuit().equals(suit)){
				listBriscole.add(c);
			}
		}
		return listBriscole;
		
	}
	private int findMinimumCard(List<Card> hand, String suit) {
		Card min = new Card("NoSuit","8"," ");
		int counter=0;
		int cardToPlay=new Random().nextInt(hand.size());
		for(Card c:hand){
			if(!c.getSuit().equals(suit) && (Integer.parseInt(c.getNumber())<Integer.parseInt(min.getNumber()))){
				min=c;
				cardToPlay=counter;
			}
			counter++;
		}
		if(min.getSuit().equals("NoSuit")){ //se non � stata trovata carta minima (che non fosse carico n� briscola)
			if(isAnyBriskInHand(hand,suit)){ //se ci sono briscole oltre ai carichi gioca la minima (anche nel caso fosse l'asso)
				cardToPlay=findMinimumBriscola(hand, suit);
			}else{ //se hai tutti carichi in mano gioca il minimo
				
			}
		}
		return cardToPlay;
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// set player1 and force a refresh with Client Pull
		if(player1 == null){
			player1 = new Player(request.getParameter("player_name"));
			
		
			if(request.getParameter("opponent").equals("human")){
				response.getWriter().println("Welcome " + player1.getName());
				response.setHeader("Refresh", "2; URL=http://"+ip+":"+port+"/Briscola_Due_Servlet/BriscolaDueServlet?player=" + player1.getName());
				response.getWriter().println("Waiting for another player");
			}
			else {
				play_with_AI = true;
				player2 = new Player("Jarvis");
				g = new Game(player1,player2);
				winner = g.endTurn();
				current = g.getTurn();
				response.getWriter().println("Welcome " + player1.getName());
				response.setHeader("Refresh", "2; URL=http://"+ip+":"+port+"/Briscola_Due_Servlet/BriscolaDueServlet?player=" + player1.getName());
				response.getWriter().println("Waiting for another player");
			}
		}
		
		// set player2 and force a refresh with Client Pull
		else if(player2 == null){
			player2 = new Player(request.getParameter("player_name"));
			g = new Game(player1,player2);
			winner = g.endTurn();
			current = g.getTurn();
			response.getWriter().append("Welcome " + player2.getName());
			response.setHeader("Refresh", "2; URL=http://"+ip+":"+port+"/Briscola_Due_Servlet/BriscolaDueServlet?player=" + player2.getName());
			response.getWriter().println("Waiting for another player");
		}
		
		// Two players limit reached
		else {
			response.getWriter().append("Players limit reached");
		}
	}


}
