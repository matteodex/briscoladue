package briscola;

public class Card implements Comparable<Card>{

	private String suit;
	private String number;
	private String url_image;
	private String back;
	private int score=0;
	
	public Card(String s, String n, String url){
		this.suit = s;
		this.number = n;
		this.url_image = url;
		this.back="http://"+ BriscolaDueServlet.ip + ":"+BriscolaDueServlet.port+"/Briscola_Due_Servlet/CarteNapoletane/back.png";
		switch(n){
			case("9"):
				this.score=11;
				break;
			case("8"):
				this.score=10;
				break;
			case("7"):
				this.score=4;
				break;

			case("6"):
				this.score=3;
				break;

			case("5"):
				this.score=2;
				break;

		}

	}
	
	public String getName(){
		return this.suit + this.number;
	}
	public String getSuit(){
		return this.suit;
	}
	public String getNumber(){
		return this.number;
	}

	@Override
	public int compareTo(Card o) {
		
		String briscola = Game.getBriscola();
		
		int n1 = Integer.parseInt(this.number);
		int n2 = Integer.parseInt(o.number);
		
		String s1 = this.suit;
		String s2 = o.suit;
		
		if(this.suit.equals(o.suit)){
			return n1 - n2;
		}
		else if(s1.equals(briscola)){
			return 1;
		}
		else if(s2.equals(briscola)){
			return -1;
		}
		else{
			return 1;
		}
	}

	public int getScore() {
		// TODO Auto-generated method stub
		return this.score;
	}
}
