package briscola;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Deck {

	private List<Card> cards;
	
	public Deck(){
		
		this.cards = new ArrayList<Card>();
		String ip = BriscolaDueServlet.ip;
		String port = BriscolaDueServlet.port;
		for(int i=0; i<40; i++){
			if(i < 10)
				this.cards.add(new Card("Ori",Integer.toString(i),"http://"+ ip + ":"+port+"/Briscola_Due_Servlet/CarteNapoletane/Ori"+(i+1)+".png"));
			else if(i < 20)
				this.cards.add(new Card("Spade",Integer.toString(i-10),"http://"+ip+":"+port+"/Briscola_Due_Servlet/CarteNapoletane/Spade"+(i-9)+".png"));
			else if(i < 30)
				this.cards.add(new Card("Bastoni",Integer.toString(i-20),"http://"+ip+":"+port+"/Briscola_Due_Servlet/CarteNapoletane/Bastoni"+(i-19)+".png"));
			else
				this.cards.add(new Card("Coppe",Integer.toString(i-30),"http://"+ip+":"+port+"/Briscola_Due_Servlet/CarteNapoletane/Coppe"+(i-29)+".png"));
		}
	}
	
	public void shuffle(){
		Card tmp;
		for(int l=0; l<3; l++){
			Random rdm = new Random();
			for(int i=0; i<40; i++){
				tmp=this.cards.remove(rdm.nextInt(40));
				this.cards.add(tmp);
			}
		}
	}
	
	public Card giveCard(){
		return this.cards.remove(0);
	}
	
	public String printDeck(){
		String s = new String();
		for(int i=0; i<cards.size(); i++){
			s = s + cards.get(i).getName() + " ";
		}
		return s;
	}
	
	public boolean isEmpty(){
		return this.cards.isEmpty();
	}

	public int getCardsLeft(){
		return this.cards.size();
	}
	public List<Card> getCards(){
		return this.cards;
	}
	public Card decideBriscola() {
		
		return this.cards.get(cards.size()-1);
	}
}
