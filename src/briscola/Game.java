package briscola;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonElement;

public class Game {
		
	private List<Player> players;
	private Deck deck;
	private List<Card> board;
	private List<Player> turn;
	private int turn_count = 0;
	
	private static String briscola;
	private Card briscola_card;
	
	public Game(Player p1, Player p2){
		this.players = new ArrayList<Player>();
		this.players.add(p1);
		this.players.add(p2);
		this.deck = new Deck();
		this.board = new ArrayList<Card>();
		this.deck.shuffle();
		int in;
		for(in=0;in<40;in++)
			System.out.println(deck.getCards().get(in).getName());
		for(int i=0; i<3; i++){
			players.get(0).getCard(deck.giveCard());
			players.get(1).getCard(deck.giveCard());
		}
		this.briscola_card = deck.decideBriscola();
		this.briscola = this.briscola_card.getSuit();
		this.turn = new ArrayList<Player>();
		turn.add(players.get(0));
		turn.add(players.get(1));
	}
	
	public Player getPlayerOne() {
		return this.players.get(0);
	}
	
	public Player getPlayerTwo() {
		return this.players.get(1);
	}
	
	public Deck getDeck(){
		return this.deck;
	}
	
	
	public String printBoard(){
		String s = new String();
		Iterator<Card> it = board.iterator();
		while(it.hasNext()){
			s += it.next().getName();
		}
		return s;
	}
	
	
	
	public Player endTurn(){
		//System.out.println("End turn, turncount = " + turn_count);
		if(turn_count == 2){
			int total = points();
			int res;
			boolean player1Won = false;
			Card first=board.remove(0);
			Card second = board.remove(0);

			res = first.compareTo(second);
			
			if((res > 0 && players.get(0)==turn.get(0)) || 
					res < 0 && players.get(0)==turn.get(1)){
				player1Won=true;
			}
			
			
			
			if(player1Won){ //ha vinto player1
				players.get(0).addPoints(total);
				turn.clear();
				turn.add(players.get(0));
				turn.add(players.get(1));
				if(!deck.isEmpty()){
					players.get(0).getCard(deck.giveCard());
					players.get(1).getCard(deck.giveCard());
				}
				turn_count = 0;
				return players.get(0);
			}else { //ha vinto player2
				players.get(1).addPoints(total);
				turn.clear();
				turn.add(players.get(1));
				turn.add(players.get(0));
				if(!deck.isEmpty()){
					players.get(1).getCard(deck.giveCard());
					players.get(0).getCard(deck.giveCard());
				}
				turn_count = 0;
				return players.get(1);
			}
		}
		return null;
	}
	
	public Player getTurn(){
		return turn.get(turn_count);
		
	}
	
	public void playTurn(int i){
				
		board.add(turn.get(turn_count).playCard(i));
		turn_count++;
	}

	

	public boolean canPlayACard() {
		return !turn.get(0).emptyHand();
	}
	
	public int points(){
		int sum = 0;
		Iterator<Card> it = board.iterator();
		while(it.hasNext()){
			sum+=it.next().getScore();
		}
		return sum;
	}

	public List<Card> getBoard() {
		
		return this.board;
	}

	public static String getBriscola() {
		return Game.briscola;
		
	}
	
	public void setBriscola(String b) {
		Game.briscola = this.briscola_card.getName();
	}
	
	public Card getBriscola_card() {
		return briscola_card;
	}

	public void setBriscola_card(Card briscola_card) {
		this.briscola_card = briscola_card;
	}

	public int getTurnCount() {
		// TODO Auto-generated method stub
		return this.turn_count;
	}

	public int numBriscolePlayed() {
		Deck listaCarte = this.getDeck();
		int countBrisk=0;
		for(Card c : listaCarte.getCards()){
			if(c.getSuit().equals(briscola_card.getSuit())){
				countBrisk++;
			}
		}
		return 10-countBrisk;
	}
	
	
}
