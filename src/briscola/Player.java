package briscola;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Player{
	
	private String name;
	private List<Card> hand;
	private int score;
	
	public Player(String name){
		score = 0;
		this.name = name;
		hand = new ArrayList<Card>();
	}
	
	public String getName(){
		return this.name;
	}
	
	public String printHand(){
		String s = new String();
		
		
		
		Iterator<Card> it = hand.iterator();
		while(it.hasNext()){
			s = s + " " + it.next().getName();
		}
		return s;
		
	}
	
	public boolean emptyHand(){
		return hand.isEmpty();
	}
	
	public void getCard(Card c){
		this.hand.add(c);
		Collections.sort(hand);
	}
	
	public Card playCard(int i){
		return this.hand.remove(i);
	}
	
	public void addPoints(int p){
		this.score += p;
	}
	
	public int getPoints(){
		return score;
	}

	public List<Card> getHand() {
		// TODO Auto-generated method stub
		Collections.sort(hand);
		return this.hand;
	}
	
	public List<Card> sizeHand(){
		List<Card> opponentHand = hand;
		return opponentHand;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name=name;
		
	}


}
